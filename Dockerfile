FROM ubuntu:latest

RUN apt-get update && apt-get install -y --no-install-recommends \
		ca-certificates \
		curl \
		wget \
        git \
        vim \
        ssh \
        openssh-server\
        sshpass \
        rsyslog \
        python-dev\
        python-numpy \ 
        python-scipy \
        python-matplotlib \
        ipython \
        ipython-notebook \
        python-pandas \
        python-sympy \
        python-nose \
        build-essential\
	&& rm -rf /var/lib/apt/lists/*

RUN wget -O /tmp/get-pip.py https://bootstrap.pypa.io/get-pip.py
RUN python /tmp/get-pip.py

ADD r.txt /
RUN pip install -r /r.txt

RUN apt-get update && apt-get install -y zsh

#Minimize image size
RUN (apt-get autoremove -y; \
     apt-get clean -y)

WORKDIR /
ADD init.sh /

ENTRYPOINT sh init.sh
