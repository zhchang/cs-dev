set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'
Plugin 'zhchang/quick_file'
Plugin 'fatih/vim-go'
Plugin 'nvie/vim-flake8'
"Plugin 'Valloric/YouCompleteMe'
call vundle#end()

syntax on
filetype indent plugin on
set modeline
set tabstop=4
set expandtab
set shiftwidth=4
set softtabstop=4

let g:go_fmt_command = "goimports"

set background=dark
set number
set guifont=Monaco:h12
nnoremap <silent> gr :GoReferrers<cr>
let g:QF_ASP='~/work/go/src/github.com/myteksi/go'
